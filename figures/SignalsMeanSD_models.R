#################################################################################################
# This file reads result files in enumerated directories, calculates mean and SD of a quantity
# and merges three different sources into a single graph.
# Works for all plots based on output files with columns for time and other value.
# Time is plotted horizontally, the other quantity vertically.
#################################################################################################
library(ggplot2)
suppressMessages(library(Hmisc))
options(warn=-1)
#################################################################################################
## @brief Extract data for a single GC 
## @param gcpath string path to data files for germinal center
## @return data.frame 
get_data <- function(gcpath,file) {
  print(paste(" --> ",gcpath,file,sep=""))
  cdf <- read.table(paste(gcpath,file, sep=""), header=FALSE, skip=tobeskipped)
  cols <- c(2,3,4,5)
  cdf <- cdf[,cols]
  names(cdf) <- c("time","mean","sd","n")
  cdf
}
#################################################################################################
mkplot <- function(dat,maxt,maxsig,xl,yl,linecol,first) {
  errcol <- linecol
  if (first==T) {
    par(mai=c(1,1,0.5,0.5), col="black")
    par(new=T)
    plot(dat$time, dat$mean, type="l", lty=1, lwd=2, col=linecol, 
         xlim=c(0,maxt), ylim=c(0,maxsig),
         cex.lab=2.3, cex.axis=2.0, xlab=xl, ylab=yl)
  } else {
    par(new=T)
    plot(dat$time, dat$mean, type="l", lty=1, lwd=2, col=linecol, 
         xlim=c(0,maxt), ylim=c(0,maxsig),
         cex.lab=2.3, cex.axis=2.0, xlab=NA, ylab=NA, axes=F)
  }  
  par(new=T)
  errbar(dat$time, dat$mean, dat$mean-dat$sd, dat$mean+dat$sd,
         type="p", cex=0.1, pch=0, cap=0.007, lty=1, lwd=1.2,
         col=errcol, errbar.col=errcol, 
         xlim=c(0,maxt), ylim=c(0,maxsig),
         xlab=NA, ylab=NA, add=TRUE, axes=F)
  par(new=T)
  plot(dat$time, dat$mean, type="l", lty=1, lwd=2, col=linecol, 
       xlim=c(0,maxt), ylim=c(0,maxsig),
       cex.lab=2.3, cex.axis=2.0, axes=F, xlab=NA, ylab=NA)
}
#################################################################################################
mkfigure <- function(sourcefile,sourcepath,targetfile,yaxislabel,dolegend) {
  # get disedi default
  disedipath <- paste("~/Work/gc-pathways/hyphasma/results_final/fcrb122f",
                      sourcepath,"default/",sep="")
  disedidef <- get_data(disedipath,sourcefile)
  # get disedi hadec205
  disedipath <- paste("~/Work/gc-pathways/hyphasma/results_final/fcrb122f",
                      sourcepath,"hadec205/",sep="")
  disedidec <- get_data(disedipath,sourcefile)
  # get mixed default
  mixedpath <- paste("~/Work/gc-pathways/hyphasma/results_final/fcrb123c",
                     sourcepath,"default/",sep="")
  mixeddef <- get_data(mixedpath,sourcefile)
  # get mixed hadec205
  mixedpath <- paste("~/Work/gc-pathways/hyphasma/results_final/fcrb123c",
                     sourcepath,"hadec205/",sep="")
  mixeddec <- get_data(mixedpath,sourcefile)
  #####
  maxt <- max(disedidef$time, disedidec$time, mixeddef$time, mixeddec$time)
  maxsig <- max(disedidef$mean+disedidef$sd, disedidec$mean+disedidec$sd, 
                mixeddef$mean+mixeddef$sd, mixeddec$mean+mixeddec$sd)
  # plot
  epsfile <- paste(targetfile,".eps",sep="")
  print(epsfile)
  cairo_ps(width=10, height=10, epsfile)
  mkplot(mixeddef, maxt, maxsig, "Time [hours]", yaxislabel, "cyan", TRUE)
  mkplot(disedidef, maxt, maxsig, "Time [hours]", yaxislabel, "grey", FALSE)
  mkplot(mixeddec, maxt, maxsig, "Time [hours]", yaxislabel, "blue", FALSE)
  mkplot(disedidec, maxt, maxsig, "Time [hours]", yaxislabel, "black", FALSE)
  if (dolegend) {
    linelegend <- c("DisseD reference", "DisseD DEC205", "Mixed reference", "Mixed DEC205")
    collegend <- c("grey", "black", "cyan", "blue")
    legend("topright", linelegend, col=collegend, lty=1, lwd=4, cex=2.0)
  }
  garbage <- dev.off()
}
#################################################################################################
tobeskipped=1
# mkfigure(name of sourcefile, sourcepath, and targetfile, columns to read, label on y-axis
mkfigure("mTORMeanSD_day6.out","_benchmark/","mTORMeanSD_day6",
         "B cell mTOR from first antigen uptake",F)
mkfigure("FoxOaMeanSD_day6.out","_benchmark/","FoxOMeanSD_day6",
         "B cell FoxO1 from first antigen uptake",T)
mkfigure("c-MycMeanSD_day6.out","_benchmark/","cMycMeanSD_day6",
         "B cell c-Myc from first antigen uptake",F)
#################################################################################################

