#################################################################################################
# This file reads result files in enumerated directories, calculates mean and SD of a quantity.
# Works for all plots based on output files with columns for time and other value.
# Time is plotted horizontally, the other quantity vertically.
#################################################################################################
library(ggplot2)
suppressMessages(library(Hmisc))
options(warn=-1)
#################################################################################################
## @brief Extract data for a single GC 
## @param gcpath string path to data files for germinal center
## @return data.frame 
get_singlesim_data <- function(gcpath,file,colnames) {
  filecomplete <- paste(gcpath, "/", file, ".out", sep="")
#  print(filecomplete)
  cdf <- read.table(filecomplete, header=FALSE, skip=tobeskipped)
  cdf <- cdf[cdf$V1>=48,]
#  cdf <- cdf[cdf$V1<125,]
  if (file == "dec205") {
    decpos <- c((cdf$V5-cdf$V23)/(cdf$V8-cdf$V20+cdf$V23))
    decneg <- c((cdf$V6-cdf$V24)/(cdf$V9-cdf$V21+cdf$V24))
    allBC <- c((cdf$V5+cdf$V6-cdf$V23-cdf$V24)/(cdf$V8+cdf$V9-cdf$V20-cdf$V21+cdf$V23+cdf$V24))
  }
  maxcol <- ncol(cdf)
  cdf <- cdf[,-(2:maxcol),drop=FALSE]
  cdf <- cbind(cdf, allBC, decpos, decneg)
  names(cdf) <- c("Time", colnames)
  cdf
}
#################################################################################################
## @brief Read data for all GCs in one in silico experiment
## @param folder string folder with one subfolder for each GC
## @return data.frame 
read_data <- function(folder,file,colnames) {
  print(paste(folder,file))
  count=0
#  limitreplicates=4
  mynames <- c("time")
  for (i in 1:length(colnames)) { mynames <- c(mynames, paste(colnames[i],"0",sep="")) }
  GCPaths <- list.dirs(path = folder, full.names = TRUE, recursive = FALSE)
  for (ifolder in GCPaths) {
     if (count<limitreplicates) {
       count=count+1
       if (count==1) { 
	  AllDataSets <- get_singlesim_data(ifolder,file,colnames)
       } else {
	 next_data <- get_singlesim_data(ifolder,file,colnames)
         next_data <- next_data[-1]
         AllDataSets <- cbind(AllDataSets,next_data)
         for (i in 1:length(colnames)) { 
           mynames <- c(mynames, paste(colnames[i],count-1,sep="")) 
         }
       }
     }
  }
  names(AllDataSets) <- mynames
  AllDataSets
}
#################################################################################################
read_all_sims <- function(path,file,colnames) {
  # Returns a data frame with data from file <file>.
  infile <- paste(file,".out",sep="")
  collected <- read_data(path, file, colnames)
  collected
}
#################################################################################################
get_mean <- function(dat,varnames) {
# expects a data format of (x, var1:0, var2:0, ..., varN:0, var1:1, ... varM:N)
# so N denotes the number of variables saved in dat in subsequent columns
# and M(maxrepeat) the number of repetitions with these N variables in further sets of N columns
# varnames contains the names of the N variables
# Note that the x variable is just once in the first column.
# Returned is the columns: (x, var1_mean, var1_sd, var2_mean, ... varM_sd)
  N <- length(varnames)
  dtcollect <- data.frame(0)
  dtcollectnames <- c("time")
  for (ni in 1:N) { 
    dtcollect <- cbind(dtcollect,0,0)
    dtcollectnames <- cbind(dtcollectnames, paste(varnames[ni],"mean",sep=""), 
                                            paste(varnames[ni],"sd",sep=""))
  }
  names(dtcollect) <- dtcollectnames
  totalmaxcol <- ncol(dat)
  maxrepeat <- (totalmaxcol-1)/N
  maxrow <- nrow(dat)
  for (i in 1:maxrow) { # run through x-values
    tmp <- c(dat$time[i])
    for (ni in 1:N) { # run through the N variables
      s = 0
      for (j in 1:maxrepeat) { s = s + dat[i,(j-1)*N+ni+1] }
      s = s / (maxrepeat-1)
      d = 0
      for (j in 1:maxrepeat) { d = d + ((dat[i,(j-1)*N+ni+1]-s)*(dat[i,(j-1)*N+ni+1]-s)) }
      d = d/(maxrepeat-1)
      d = sqrt(d)
      tmp <- cbind(tmp, s, d)
    }
    dtcollect <- rbind(dtcollect, tmp[1,])
  }
  # remove the initial zero line
  dtcollect <- dtcollect[-1,]
  dtcollect
}
#################################################################################################
mkplot <- function(dat, colnames, vcol, mintime, maxtime, minval, maxval,
                   xl, yl, first) {
  n <- length(colnames)
  if (first == 1) { 
    par(mai=c(1,1,0.5,0.5), col="black") 
    plot(0,0, type="l", lty=1, lwd=2, col="white", 
         xlim=c(mintime,maxtime), ylim=c(minval,maxval),
         cex.lab=2.3, cex.axis=2.0, xlab=xl, ylab=yl)
  }
  for (i in 2:length(colnames)) {
    if ( first < 2 ) {
      par(new=T)
      errbar(dat[,1]/24, dat[,(2*i)], dat[,(2*i)]-dat[,(2*i+1)], dat[,(2*i)]+dat[,(2*i+1)],
             type="p", cex=0.1, pch=0, cap=0.007, lty=1, lwd=1.2,
             col=vcol[2], errbar.col=vcol[2], 
             xlim=c(mintime,maxtime), ylim=c(minval,maxval),
             xlab=NA, ylab=NA, add=TRUE, axes=F)
    }
    par(new=T)
    plot(dat$time/24, dat[,(2*i)], type="l", lty=i-1, lwd=3, col=vcol[1], 
         xlim=c(mintime,maxtime), ylim=c(minval,maxval),
         cex.lab=2.3, cex.axis=2.0, axes=F, xlab=NA, ylab=NA)
  }
}
#################################################################################################
mkfigurethis <- function(sourcepath,sourcefile,colnames,colmodel,yaxislabel,first) {
  n <- length(colnames)
  thispath <- paste(sourcepath,"x-all",sep="")
  mydata <- read_all_sims(thispath,sourcefile,colnames)
  mystat <- get_mean(mydata,colnames)
  #
  mintime <- 2
  maxtime <- 21
  minval <- 0
  maxval <- 8
  # plot
  mkplot(mystat, colnames, colmodel, mintime, maxtime, minval, maxval,
         "Time [days]", yaxislabel, first)
  mystat
}

#################################################################################################
mkfigure <- function(sourcefile,colnames,yaxislabel) {
  epsfile <- "Tsc1_dz2lz_stat.eps"
  print(epsfile)
  cairo_ps(width=10, height=10, epsfile)
  # get bcintime model
  colbcintime <- c("red", "darkorange")
  bcintimepath <- "~/Work/gc-pathways/hyphasma/results_final/bcintime28k09haTsc1"
  bcintimedata <- mkfigurethis(bcintimepath,sourcefile,colnames,colbcintime,yaxislabel,1)
  # get mixed model
  colmixed <- c("blue", "deepskyblue")
  mixedpath <- "~/Work/gc-pathways/hyphasma/results_final/fcrb123chaTsc1"
  mixeddata <- mkfigurethis(mixedpath,sourcefile,colnames,colmixed,yaxislabel,0)
  # get disedi
  coldi <- c("black", "grey30")
  disedipath <- "~/Work/gc-pathways/hyphasma/results_final/fcrb122fhaTsc1"
  mkfigurethis(disedipath,sourcefile,colnames,coldi,yaxislabel,0)
  #
  # values have to match those in mkfigurethis!
  mkplot(bcintimedata,colnames,colbcintime,2,21,0,8,"Time [days]",yaxislabel,2)
  mkplot(mixeddata,colnames,colmixed,2,21,0,8,"Time [days]",yaxislabel,2)
  #
  linelegend <- c("Tsc1-deficient", "Tsc1-competent")
  collegend <- c("black", "black")
  legend("top", linelegend, col=collegend, 
         lty=c(1,2), lwd=4, cex=2.0, pt.cex=3.0)
  linelegend <- c("DisseD", "BCinTime", "MiXed")
  collegend <- c("black", "red", "blue")
  legend("topright", linelegend, col=collegend, 
         lty=c(1,1,1), lwd=4, cex=2.0, pt.cex=3.0)
  #
  garbage <- dev.off()
}
#################################################################################################
limitreplicates <- 1000
tobeskipped=1
colnames <- c("BC","DECpos","DECneg")
mkfigure("dec205",colnames,"DZ to LZ ratio")
#################################################################################################

