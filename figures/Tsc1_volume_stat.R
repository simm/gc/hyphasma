#################################################################################################
# This file reads result files in enumerated directories, calculates mean and SD of a quantity
# and merges three different sources into a single graph.
# Works for all plots based on output files with columns for time and other value.
# Time is plotted horizontally, the other quantity vertically.
#################################################################################################
library(ggplot2)
suppressMessages(library(Hmisc))
options(warn=-1)
#################################################################################################
## @brief Extract data for a single GC 
## @param gcpath string path to data files for germinal center
## @return data.frame 
get_singlesim_data <- function(gcpath,file,cnames,cols) {
  cdf <- read.table(paste(gcpath,"/",file, sep=""), header=FALSE, skip=tobeskipped)
  cdf <- cdf[,cols]
  names(cdf) <- cnames
  # If the input data are to be transformed or combined in any way, do it here:
  if (file == "tc.out") {
    maxtc <- max(cdf$vol)
    cdf$vol <- cdf$vol/maxtc
  }
  cdf
}
#################################################################################################
## @brief Read data for all GCs in one in silico experiment
## @param folder string folder with one subfolder for each GC
## @return data.frame 
read_data <- function(folder,file,cnames,cols) {
  #print(file)
  count=0
  mynames <- c("time","vol0")
  GCPaths <- list.dirs(path = folder, full.names = TRUE, recursive = FALSE)
  for (ifolder in GCPaths) {
     if (count<limitreplicates) {
       count=count+1
       if (count==1) { 
	  AllDataSets <- get_singlesim_data(ifolder,file,cnames,cols)
       } else {
	 next_data <- get_singlesim_data(ifolder,file,cnames,cols)
         AllDataSets <- cbind(AllDataSets,next_data$vol)
         mynames <- c(mynames, paste("vol",count-1,sep=""))
       }
     }
  }
  names(AllDataSets) <- mynames
  #print(names(AllDataSets))
  AllDataSets
}
#################################################################################################
mktable <- function(path, file, cnames, cols) {
  infile <- paste(file,".out",sep="")
  dtf <- read_data(path,infile,cnames,cols)
  dtf
}
#################################################################################################
read_all_sims <- function(path,file,x,y) {
  # Returns a data frame with data from file <file>.
  # It collects column x and y from the file in each enumerated subdirectory.
  # The data frame is organized with the columns x, y_i with i\in[all subdirectories]
  # Typically, x would denote the time and y an observable
  # Note that the x values have to be the same in <file> of all subdirectories.
  cols <- c(x,y)
  cnames <- c("time","vol")
  collected <- mktable(path, file, cnames, cols)
  collected
}
#################################################################################################
get_mean <- function(dat) {
  dtcollect <- data.frame(0)
  dtcollect <- cbind(dtcollect,0,0)
  names(dtcollect) <- c("time", "mean", "sd")
  maxcol <- ncol(dat)
  maxrow <- nrow(dat)
  #dtcollect <- cbind(dat$time, mean(dat[,2:maxcol]), sd(dat[,2:maxcol]))
  for (i in 1:maxrow) {
    s = 0
    for (j in 2:maxcol) { s = s + dat[i,j] }
    s = s / (maxcol-1)
    d = 0
    for (j in 2:maxcol) { d = d + ((dat[i,j]-s)*(dat[i,j]-s)) }
    d = d/(maxcol-1)
    d = sqrt(d)
    tmp <- c(dat$time[i], s, d)
    dtcollect <- rbind(dtcollect, tmp)
#    print(dtcollect)
  }
#  print(dtcollect)
  dtcollect
}
#################################################################################################
mkplot <- function(dat,maxt,maxvol,xl,yl,linest,linecol,errcol,first) {
  if (first==1) {
    par(mai=c(1,1,0.5,0.5), col="black")
    par(new=T)
    plot(dat$time/24, dat$mean, type="l", lty=linest, lwd=2, col=linecol, 
         xlim=c(0,maxt), ylim=c(0,maxvol),
         cex.lab=2.3, cex.axis=2.0, xlab=xl, ylab=yl)
  } else {
    par(new=T)
    plot(dat$time/24, dat$mean, type="l", lty=linest, lwd=2, col=linecol, 
         xlim=c(0,maxt), ylim=c(0,maxvol),
         cex.lab=2.3, cex.axis=2.0, xlab=NA, ylab=NA, axes=F)
  }  
  if (first < 2) { 
    par(new=T)
    errbar(dat$time/24, dat$mean, dat$mean-dat$sd, dat$mean+dat$sd,
           type="p", cex=0.1, pch=0, cap=0.007, lty=1, lwd=1.2,
           col=errcol, errbar.col=errcol, 
           xlim=c(0,maxt), ylim=c(0,maxvol),
           xlab=NA, ylab=NA, add=TRUE, axes=F)
    par(new=T)
    plot(dat$time/24, dat$mean, type="l", lty=linest, lwd=2, col=linecol, 
         xlim=c(0,maxt), ylim=c(0,maxvol),
         cex.lab=2.3, cex.axis=2.0, axes=F, xlab=NA, ylab=NA)
  }
}
#################################################################################################
mkfigure <- function(sourcefile,targetfile,xcol,ycol,ycol2,yaxislabel,doti) {
  # get disedi
  disedipath <- "~/Work/gc-pathways/hyphasma/results_final/fcrb122fhaTsc1x-all"
  print(disedipath)
  disedidecpos <- read_all_sims(disedipath,sourcefile,xcol,ycol)
  disedidecpos_msd <- get_mean(disedidecpos)
  disedidecneg <- read_all_sims(disedipath,sourcefile,xcol,ycol2)
  disedidecneg_msd <- get_mean(disedidecneg)
  # get mixed
  mixedpath <- "~/Work/gc-pathways/hyphasma/results_final/fcrb123chaTsc1x-all"
  print(mixedpath)
  mixeddecpos <- read_all_sims(mixedpath,sourcefile,xcol,ycol)
  mixeddecpos_msd <- get_mean(mixeddecpos)
  mixeddecneg <- read_all_sims(mixedpath,sourcefile,xcol,ycol2)
  mixeddecneg_msd <- get_mean(mixeddecneg)
  if (doti) {
    # get bcintime
    bcintimepath <- "~/Work/gc-pathways/hyphasma/results_final/bcintime28k09haTsc1x-all"
    print(bcintimepath)
    bcintimedecpos <- read_all_sims(bcintimepath,sourcefile,xcol,ycol)
    bcintimedecpos_msd <- get_mean(bcintimedecpos)
    bcintimedecneg <- read_all_sims(bcintimepath,sourcefile,xcol,ycol2)
    bcintimedecneg_msd <- get_mean(bcintimedecneg)
  }
  #	
  maxt <- max(disedidecpos_msd$time, disedidecneg_msd$time, 
              mixeddecpos_msd$time, mixeddecneg_msd$time)/24
  maxvol <- max(disedidecpos_msd$mean+disedidecpos_msd$sd, 
                disedidecneg_msd$mean+disedidecneg_msd$sd,
                mixeddecpos_msd$mean+mixeddecpos_msd$sd,
                mixeddecneg_msd$mean+mixeddecneg_msd$sd)
  if (doti) {
    maxt <- max(maxt, bcintimedecpos_msd$time/24, bcintimedecneg_msd$time/24)
    maxvol <- max(maxvol,
                  bcintimedecpos_msd$mean+bcintimedecpos_msd$sd,
                  bcintimedecneg_msd$mean+bcintimedecneg_msd$sd)
  }
  # plot
  epsfile <- paste(targetfile,".eps",sep="")
  print(epsfile)
  cairo_ps(width=10, height=10, epsfile)
  mkplot(mixeddecpos_msd, maxt, maxvol, "Time [days]", yaxislabel, 1, "blue", "cyan", 1)
  if (doti) {
    mkplot(bcintimedecpos_msd, maxt, maxvol, "Time [days]", yaxislabel, 1, "red", "orange", 0)
  }
  mkplot(disedidecpos_msd, maxt, maxvol, "Time [days]", yaxislabel, 1, "black", "grey", 0)
  mkplot(mixeddecneg_msd, maxt, maxvol, "Time [days]", yaxislabel, 2, "blue", "cyan", 0)
  if (doti) {
    mkplot(bcintimedecneg_msd, maxt, maxvol, "Time [days]", yaxislabel, 2, "red", "orange", 0)
  }
  mkplot(disedidecneg_msd, maxt, maxvol, "Time [days]", yaxislabel, 2, "black", "grey", 0)
  mkplot(mixeddecpos_msd, maxt, maxvol, "Time [days]", yaxislabel, 1, "blue", "cyan", 2)
  mkplot(mixeddecneg_msd, maxt, maxvol, "Time [days]", yaxislabel, 2, "blue", "cyan", 2)
  #
  linelegend <- c("Tsc1-deficient", "Tsc1-competent")
  collegend <- c("black", "black")
  legend("topright", linelegend, col=collegend, lty=c(1,2), lwd=4, cex=2.0, pt.cex=3.0)
  linelegend <- c("DisseD", "Mixed")
  collegend <- c("black", "blue")
  if (doti) {
    linelegend <- c("DisseD", "BCinTime", "Mixed")
    collegend <- c("black", "red", "blue")
  }
  legend("right", linelegend, col=collegend, lty=1, lwd=4, cex=2.0)
  garbage <- dev.off()
}
#################################################################################################
limitreplicates <- 100
tobeskipped=1
includeBCinTime=TRUE
# mkfigure(name of sourcefile, sourcepath, and targetfile, columns to read, label on y-axis
mkfigure("dec205","Tsc1_volume_stat",1,2,3,"Number of GC B cells",includeBCinTime)
#################################################################################################

