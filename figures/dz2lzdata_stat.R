#################################################################################################
# This file reads result files in enumerated directories, calculates mean and SD of a quantity.
# Works for all plots based on output files with columns for time and other value.
# Time is plotted horizontally, the other quantity vertically.
#################################################################################################
library(ggplot2)
suppressMessages(library(Hmisc))
options(warn=-1)
#################################################################################################
## @brief Extract data for a single GC 
## @param gcpath string path to data files for germinal center
## @return data.frame 
get_singlesim_data <- function(gcpath,file,colnames) {
  filecomplete <- paste(gcpath, "/", file, ".out", sep="")
#  print(filecomplete)
  cdf <- read.table(filecomplete, header=FALSE, skip=tobeskipped)
  cdf <- cdf[cdf$V1>=120,]
#  cdf <- cdf[cdf$V1<125,]
  if (file == "dec205") {
    decpos <- c((cdf$V5-cdf$V23)/(cdf$V8-cdf$V20+cdf$V23))
    decneg <- c((cdf$V6-cdf$V24)/(cdf$V9-cdf$V21+cdf$V24))
    allBC <- c((cdf$V5+cdf$V6-cdf$V23-cdf$V24)/(cdf$V8+cdf$V9-cdf$V20-cdf$V21+cdf$V23+cdf$V24))
  }
  maxcol <- ncol(cdf)
  cdf <- cdf[,-(2:maxcol),drop=FALSE]
  cdf <- cbind(cdf, allBC, decpos, decneg)
  names(cdf) <- c("Time", colnames)
  cdf
}
#################################################################################################
## @brief Read data for all GCs in one in silico experiment
## @param folder string folder with one subfolder for each GC
## @return data.frame 
read_data <- function(folder,file,colnames) {
  print(paste(folder,file))
  count=0
#  limitreplicates=4
  mynames <- c("time")
  for (i in 1:length(colnames)) { mynames <- c(mynames, paste(colnames[i],"0",sep="")) }
  GCPaths <- list.dirs(path = folder, full.names = TRUE, recursive = FALSE)
  for (ifolder in GCPaths) {
     if (count<limitreplicates) {
       count=count+1
       if (count==1) { 
	  AllDataSets <- get_singlesim_data(ifolder,file,colnames)
       } else {
	 next_data <- get_singlesim_data(ifolder,file,colnames)
         next_data <- next_data[-1]
         AllDataSets <- cbind(AllDataSets,next_data)
         for (i in 1:length(colnames)) { 
           mynames <- c(mynames, paste(colnames[i],count-1,sep="")) 
         }
       }
     }
  }
  names(AllDataSets) <- mynames
  AllDataSets
}
#################################################################################################
read_all_sims <- function(path,file,colnames) {
  # Returns a data frame with data from file <file>.
  infile <- paste(file,".out",sep="")
  collected <- read_data(path, file, colnames)
  collected
}
#################################################################################################
get_mean <- function(dat,varnames) {
# expects a data format of (x, var1:0, var2:0, ..., varN:0, var1:1, ... varM:N)
# so N denotes the number of variables saved in dat in subsequent columns
# and M(maxrepeat) the number of repetitions with these N variables in further sets of N columns
# varnames contains the names of the N variables
# Note that the x variable is just once in the first column.
# Returned is the columns: (x, var1_mean, var1_sd, var2_mean, ... varM_sd)
  N <- length(varnames)
  dtcollect <- data.frame(0)
  dtcollectnames <- c("time")
  for (ni in 1:N) { 
    dtcollect <- cbind(dtcollect,0,0)
    dtcollectnames <- cbind(dtcollectnames, paste(varnames[ni],"mean",sep=""), 
                                            paste(varnames[ni],"sd",sep=""))
  }
  names(dtcollect) <- dtcollectnames
  totalmaxcol <- ncol(dat)
  maxrepeat <- (totalmaxcol-1)/N
  maxrow <- nrow(dat)
  for (i in 1:maxrow) { # run through x-values
    tmp <- c(dat$time[i])
    for (ni in 1:N) { # run through the N variables
      s = 0
      for (j in 1:maxrepeat) { s = s + dat[i,(j-1)*N+ni+1] }
      s = s / (maxrepeat-1)
      d = 0
      for (j in 1:maxrepeat) { d = d + ((dat[i,(j-1)*N+ni+1]-s)*(dat[i,(j-1)*N+ni+1]-s)) }
      d = d/(maxrepeat-1)
      d = sqrt(d)
      tmp <- cbind(tmp, s, d)
    }
    dtcollect <- rbind(dtcollect, tmp[1,])
  }
  # remove the initial zero line
  dtcollect <- dtcollect[-1,]
  dtcollect
}
#################################################################################################
mkplot <- function(dat, colnames, exp, xl, yl, toptitle) {
  par(mai=c(1,1,0.5,0.5), col="black")
  par(new=T)
  n <- length(colnames)
  mintime <- min(dat$time, exp[,1])
  maxtime <- max(dat$time, exp[,1])
  minval <- 0
  # note that for log values use 3,5 in exp rather than 2,4
  dattmp <- na.omit(dat)   #dat[is.finite(dat)]
  maxval <- max(dattmp[,(2:(2*n+1))], exp[,2], exp[,4])
  linecol <- c("sienna4","green3","magenta3")
  errcol <- c("sienna2","green1","magenta1")
  datacol <- c("sienna4","green4","magenta4")
  plot(dat$time, dat[,2], type="l", lty=1, lwd=2, col=linecol[1], 
       xlim=c(mintime,maxtime), ylim=c(minval,maxval),
       cex.lab=2.3, cex.axis=2.0, cex.main=2.5,
       xlab=xl, ylab=yl, main=toptitle)
  for (i in 1:length(colnames)) {
    par(new=T)
    errbar(dat[,1], dat[,(2*i)], dat[,(2*i)]-dat[,(2*i+1)], dat[,(2*i)]+dat[,(2*i+1)],
           type="p", cex=0.1, pch=0, cap=0.007, lty=1, lwd=1.2,
           col=errcol, errbar.col=errcol[i], 
           xlim=c(mintime,maxtime), ylim=c(minval,maxval),
           xlab=NA, ylab=NA, add=TRUE, axes=F)
    par(new=T)
    plot(dat$time, dat[,(2*i)], type="l", lty=1, lwd=3, col=linecol[i], 
         xlim=c(mintime,maxtime), ylim=c(minval,maxval),
         cex.lab=2.3, cex.axis=2.0, axes=F, xlab=NA, ylab=NA)
  }  
  par(new=T)
  plot(exp[,1], exp[,4], type="p", pch=19, lwd=2, col=datacol[3], 
       xlim=c(mintime,maxtime), ylim=c(minval,maxval), cex=4.0,
       cex.lab=2.3, cex.axis=2.0, axes=F, xlab=NA, ylab=NA)
  par(new=T)
  plot(exp[,1], exp[,2], type="p", pch=17, lwd=2, col=datacol[2], 
       xlim=c(mintime,maxtime), ylim=c(minval,maxval), cex=4.0,
       cex.lab=2.3, cex.axis=2.0, axes=F, xlab=NA, ylab=NA)
}
#################################################################################################
read_expdata <- function() {
  expfile <- "~/Work/gc-pathways/hyphasma/data/victora10cell-fig6c.exp"
  print(expfile)
  cdf <- read.table(expfile, header=FALSE)
  names(cdf) <- c("Time", "DECpos", "DECposlog", "DECneg", "DECneglog")
  cdf
}
#################################################################################################
mkfigurethis <- function(sourcepath,sourcefile,targetfile,colnames,yaxislabel,toptitle,dolegend) {
  # get experimental data
  expdata <- read_expdata()
  # get simulation data
  mydata <- read_all_sims(sourcepath,sourcefile,colnames)
  mystat <- get_mean(mydata,colnames)
  # plot
  epsfile <- paste(targetfile,".eps",sep="")
  print(epsfile)
  cairo_ps(width=10, height=10, epsfile)
  mkplot(mystat, colnames, expdata, "Time [hours]", yaxislabel,toptitle)
  if (dolegend) {
    linelegend <- c("all B cells", "DEC205-pos", "DEC205-neg", 
                    "data DEC205-pos", "data DEC205-neg")
    collegend <- c("sienna4", "green3", "magenta3", "green4", "magenta4")
    legend("topleft", linelegend, col=collegend, 
           lty=c(1,1,1,NA,NA), pch=c(NA,NA,NA,17,19), 
           lwd=4, cex=2.0, pt.cex=3.0)
  }
  garbage <- dev.off()
}

#################################################################################################
mkfigure <- function(sourcefile,colnames,yaxislabel) {
  # get disedi
  disedipath <- "~/Work/gc-pathways/hyphasma/results_final/fcrb122fhadec205x-all"
  targetfile <- "dz2lz_disedi_stat"
  toptitle <- "DisseD"
  mkfigurethis(disedipath,sourcefile,targetfile,colnames,yaxislabel,toptitle,1)
  # get bcintime
  bcintimepath <- "~/Work/gc-pathways/hyphasma/results_final/bcintime28k09hadec205x-all"
  targetfile <- "dz2lz_bcintime_stat"
  toptitle <- "BCinTime"
  mkfigurethis(bcintimepath,sourcefile,targetfile,colnames,yaxislabel,toptitle,0)
  # get mixed model
  mixedpath <- "~/Work/gc-pathways/hyphasma/results_final/fcrb123chadec205x-all"
  targetfile <- "dz2lz_mixed_stat"
  toptitle <- "MiXed"
  mkfigurethis(mixedpath,sourcefile,targetfile,colnames,yaxislabel,toptitle,0)
}
#################################################################################################
limitreplicates <- 1000
tobeskipped=1
colnames <- c("BC","DECpos","DECneg")
mkfigure("dec205",colnames,"DZ to LZ ratio")
#################################################################################################

