#################################################################################################
library(ggplot2)
suppressMessages(library(Hmisc))
options(warn=-1)
##################################################################################################
# generates visualisation of the histo_monitor readout of hyphasma
# generates a histogram on log scale and mean+SD plot over the time of the GCR
# Data are collected from the respective *.out files generated with histo_monitor
# in the mode of dayly resolution:
# column 1 (bin, each line is one bin of the histo_monitor readout), 
# column 2 (readout level corresponding to the bin) 
# column 3 (total frequency of occurrence),
# column 4-25 (frequencies at days)
##################################################################################################
get_data <- function(filename) {
  print(filename)
  dataset <- read.table(filename, header=FALSE, skip=1)
  colnumber <- ncol(dataset) - 4
  resultname <- c("BIN","SIG","FREQ")
  resultname <- c(resultname, sprintf("D%i", 0:colnumber))
  names(dataset) <- resultname
  dataset
}
#################################################################################################
get_mean <- function(dat) {
  dtcollect <- data.frame(0)
  dtcollect <- cbind(dtcollect,0,0)
  names(dtcollect) <- c("time", "mean", "sd")
  # Day 0 is in col 4!
  maxdays <- ncol(dat)-4
  maxSIG <- nrow(dat)
  for (i in 0:maxdays) {
    s = 0
    n = 0
    for (j in 1:maxSIG) { 
      s = s + dat[j,i+4]*dat[j,2] 
      n = n + dat[j,i+4]
    }
    if ( n > 0 ) { s = s / n } # else s = 0 anyway
    # now s contains mean and n the number of occurrences
    # work on SD:
    d = 0
    for (j in 1:maxSIG) { d = d + dat[j,i+4]*((dat[j,2]-s)*(dat[j,2]-s)) }
    if (n > 1) {
      d = d / (n - 1)
      d = sqrt(d)
    } else { d = 0 }
    tmp <- c(i, s, d)
    dtcollect <- rbind(dtcollect, tmp)
  }
  dtcollect
}
#################################################################################################
mkplot <- function(dat, maxt, maxy, linetype, lcol, xl, yl, first, dt) {
  mintime = 0
  maxtime = maxt
  miny = 0
  if (first==0) { 
    par(new=T) 
    plot(dat$time+dt, dat$mean, type="l", lty=linetype, lwd=3, col=lcol, 
         xlim=c(mintime,maxtime), ylim=c(miny,maxy),
         cex.lab=2.3, cex.axis=2.0, xlab=NA, ylab=NA, axes=F)
  } else {
    plot(dat$time+dt, dat$mean, type="l", lty=linetype, lwd=3, col=lcol, 
         xlim=c(mintime,maxtime), ylim=c(miny,maxy),
         cex.lab=2.3, cex.axis=2.0, xlab=xl, ylab=yl)
  }
  par(new=T)
  errbar(dat$time+dt, dat$mean, dat$mean-dat$sd, dat$mean+dat$sd,
         type="p", cex=0.1, pch=0, cap=0.01, lty=1, lwd=1.6,
         col=lcol, errbar.col=lcol, 
         xlim=c(mintime,maxtime), ylim=c(miny,maxy),
         xlab=NA, ylab=NA, add=TRUE)
  par(new=T)
  plot(dat$time+dt, dat$mean, type="l", lty=linetype, lwd=3, col=lcol, 
       xlim=c(mintime,maxtime), ylim=c(miny,maxy),
       cex.lab=2.3, cex.axis=2.0, xlab=NA, ylab=NA, axes=F)
  first = 0
  first
}
#################################################################################################
mktimeplot <- function(didat.list, tidat.list, midat.list, filename, thelabel) {
  # open file for time plot
  file <- paste(filename,".eps",sep="")
  print(paste("--> ",file))
  cairo_ps(width=10, height=10, file)
  par(mai=c(1,1,0.2,0.2), col="black", lwd=2) 
  # work on the data which are saved in alldat
  diseldata <- get_mean(didat.list[[2]])
  dideldata <- get_mean(didat.list[[3]])
  tiseldata <- get_mean(tidat.list[[2]])
  tideldata <- get_mean(tidat.list[[3]])
  miseldata <- get_mean(midat.list[[2]])
  mideldata <- get_mean(midat.list[[3]])
  maxt = max(diseldata$time, dideldata$time,
             tiseldata$time, tideldata$time,
             miseldata$time, mideldata$time)
  maxy = max(diseldata$mean+diseldata$sd, dideldata$mean+dideldata$sd,
             tiseldata$mean+tiseldata$sd, tideldata$mean+tideldata$sd,
             miseldata$mean+miseldata$sd, mideldata$mean+mideldata$sd)
  # generate the graph and plot the first curve
  legendsets <- c()
  legendcols <- c()
  mkplot(diseldata, maxt, maxy, 1, "black", "Time [days]", thelabel, TRUE, 0.1)
  mkplot(dideldata, maxt, maxy, 2, "black", "Time [days]", thelabel, FALSE, -0.05)
  mkplot(tiseldata, maxt, maxy, 1, "red", "Time [days]", thelabel, FALSE, 0)
  mkplot(tideldata, maxt, maxy, 2, "red", "Time [days]", thelabel, FALSE, -0.15)
  mkplot(miseldata, maxt, maxy, 1, "blue", "Time [days]", thelabel, FALSE, 0.05)
  mkplot(mideldata, maxt, maxy, 2, "blue", "Time [days]", thelabel, FALSE, -0.1)
  linelegend <- c("selected","deleted","DisseD","BCinTime","Mixed")
  collegend <- c("grey", "grey", "black", "red", "blue")
  lines <- c(1, 2, 1 ,1 ,1 )
  legend("topleft", linelegend, col=collegend, lty=lines, lwd=4, cex=1.9)
  garbage <- dev.off()
}
##################################################################################################
work_data <- function(pathname,filebasename) {
  # save which data exist c(total, selected, deleted):
  sumdata <- data.frame()
  seldata <- data.frame()
  deldata <- data.frame()
  # build filenames:
  filenames <- c( paste(pathname,filebasename,".out",sep=""), 
                  paste(pathname,filebasename,"_selected.out",sep=""),
                  paste(pathname,filebasename,"_deleted.out",sep="") )
  # read the data
  seldata <- get_data(filenames[2])
  deldata <- get_data(filenames[3])
  alldata.list <- list(sumdata, seldata, deldata)
  alldata.list
}
#################################################################################################
## main program general:
#################################################################################################
print("nTCcontacts.R (inspired from histo_monitor.R) ...")
thisfilename <- "nTCcontacts"
thislabel <- "Number of Tfh contacts per B cell and LZ passage"
pathname <- "~/Work/gc-pathways/hyphasma/results_final/fcrb122f_benchmark/default/"
didata.list <- work_data(pathname,thisfilename)
pathname <- "~/Work/gc-pathways/hyphasma/results_final/bcintime28k09_benchmark/default/"
tidata.list <- work_data(pathname,thisfilename)
pathname <- "~/Work/gc-pathways/hyphasma/results_final/fcrb123c_benchmark/default/"
midata.list <- work_data(pathname,thisfilename)
mktimeplot(didata.list, tidata.list, midata.list, thisfilename, thislabel)
#################################################################################################

