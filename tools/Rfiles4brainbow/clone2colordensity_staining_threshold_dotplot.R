##################################################################################################################
# dotplot of clonal dominance versus staining density for different staining thresholds
##################################################################################################################
#library(ggplot2)
# set the x range
tmax=12
# set the y range
ymin=0
ymax=100
# remove the message that some data were not plotted
options(warn=-1)
##################################################################################################################
# set the number of thresholds to be used
min_list = seq(0.0,0.5,by=0.1)
for (i in 1:length(min_list)) {
  min_stained = min_list[i]
  print(paste("Run with min_stained=",min_stained,"..."))
  file=paste("clone2colordensity_staining_threshold",100*min_stained,".eps",sep="")
  file
  # set the output file name
  cairo_ps(width=15, height=8, file)
  # go through all Nruns
  dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
  count=0
  times <- c(0)
  for (idir in dir_list) { 
    count=count+1
    # read next data set
    ifile<-paste(idir,"/clonality.out", sep="")
    simall<- read.table(ifile, header=FALSE, skip=1)
    # Reduce the data to those with a minimum staining level of <threshold>
    sim <- simall[simall[,3]>min_stained*simall[,7],]
    tsim <- sim[,2]/24
    # staining level is calculated as number of stained cells divided by total number of cells
    xsim <- 100*sim[,3]/sim[,7]
    # fraction of dominant clone
    ysim <- 100*sim[,8]
    # ysim <- 100*sim[,4] # this is color dominance
    if (count==1) {
       times <- unique(simall[,2]/24)
       plot_cols <- rainbow(length(times))
       # set the margins and set the line width
       par(mai=c(1,1,0.5,0.5), col="black") 
       # generate the graph and plot the first curve
       plot(xsim, ysim, col=plot_cols[tsim+1], pch=tsim, type="p", cex.axis=1.2, 
            cex.lab=1.5, cex=1.2,  xlab="% of stained", ylab="% of clone dominant",
            xlim=c(ymin,ymax), ylim=c(ymin,ymax))
       title(main = paste("Clonal dominance versus staining density, staining threshold ",100*min_stained,sep="") )
    } else {
       # prevent opening of a new graph and set the color
       par(new=TRUE, col=FALSE)
       # plot without remaking axes
       plot(xsim, ysim, col=plot_cols[tsim+1], pch=tsim, type="p", cex=1.2, 
            xlab="", ylab="", axes=FALSE, xlim=c(ymin,ymax), ylim=c(ymin,ymax))
    }
    par(new=TRUE, col="black")
    plot(xsim, ysim, col="grey", type="l", cex=1.0, 
         xlab="", ylab="", axes=FALSE, xlim=c(ymin,ymax), ylim=c(ymin,ymax))
  }
  legend(0,100, times, pch=times, lwd=2, col=plot_cols, title="time [d]")
  dev.off()
}
##################################################################################################################
