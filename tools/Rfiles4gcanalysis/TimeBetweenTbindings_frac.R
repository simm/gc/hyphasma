##################################################################################################
# Generates a histogram of the times between two interactions wiht TFH.
# The time monitored is the time between unbinding the previous and binding the next Tfh.
# Data are collected from TimeBetweenTbindings.out 
# in column 2 (Time) and 3 (frequency) and 4-25 (frequencies at days)
##################################################################################################
get_data <- function(pathname,filename) {
  dataset <- read.table(paste(pathname,filename,sep=""), header=FALSE, skip=1)
  colnumber <- ncol(dataset) - 4
  resultname <- c("BIN","TIME","FREQ")
  resultname <- c(resultname, sprintf("D%i", 0:colnumber))
  names(dataset) <- resultname
  dataset
}

# remove the message that some data were not plotted
# options(warn=-1) 
daynumber = 21
# set the output file name
file <- "TimeBetweenTbindings_frac.eps"
print(file)
cairo_ps(width=15, height=10, file)
# read the data
alldata<- get_data("./","TimeBetweenTbindings.out")
datatmp <- alldata[ which(alldata$FREQ>0), ]
MaxTime <- max(datatmp$TIME)+1
#MaxTime <- 4
SumFreq <- sum(alldata$FREQ)
if (SumFreq > 0) {
  DataFrac <- alldata$FREQ / SumFreq
} else {
  DataFrac <- alldata$FREQ
}
MaxFreq <- max(alldata$FREQ) / SumFreq
#MaxFreq <- 0.03
par(mai=c(1.1,1.3,0.4,0.2), col="black", lwd=2) 
# generate the graph and plot the first curve
plot(alldata$TIME, DataFrac, lty=1, type="l", col="black", 
         cex.axis=2.0, cex.lab=2.5, cex=2.0, 
         xlab="", ylab="", xlim=c(0,MaxTime), ylim=c(0,MaxFreq))
title(xlab="Time between two binding events to Tfh [hours]", 
      cex=2.0, cex.lab=2.5, line=4)
title(ylab="Fraction of total", cex=2.0, cex.lab=2.5, line=4)
par(col="black")
#legend(0.5, 8e+04, inset=.02, c("dead B cells","selected B cells"), 
#       lty=c(2,1), bty="n", cex=2, horiz=FALSE)
plot_col <- rainbow(daynumber+1)
daynames <- c()
plotted_cols <- c()
daylist <- list(1,2,3,5,7,10,15,21)
#for (days in 0:21) {
for (days in daylist) {
  tmpdayname <- paste("Day ",days,sep="")
  daynames <- c(daynames,tmpdayname)
  plotted_cols <- c(plotted_cols, plot_col[days+1])
  SumFreq <- sum(alldata[,days+4])
  if (SumFreq > 0) {
    DataFrac <- alldata[,days+4] / SumFreq
  } else {
    DataFrac <- alldata[,days+4]
  }
  par(new=TRUE, lwd=2)
  plot(alldata$TIME, DataFrac, lty=1, type="l", col=plot_col[days+1],
       cex.axis=2.0, cex.lab=2.5, cex=2.0, 
       xlab="", ylab="", axes=FALSE,
       xlim=c(0,MaxTime), ylim=c(0,MaxFreq))
}
legend("topright", daynames, lty=1, lwd=2, cex=1.4, col=plotted_cols, title="GC time")
garbage <- dev.off()
##################################################################################################
