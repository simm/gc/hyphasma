##################################################################################################
# generates a plot of apoptosis events with BCs of different life time since entering the
# state FDCselected.
# Data are collected from apolog.out in column 1 (time) and 7 (FDCselected_clock)
##################################################################################################
get_data <- function(pathname,filename) {
  dataset <- read.table(paste(pathname,filename,sep=""), header=FALSE, skip=1)
  names(dataset) <- c("days","state","nFDCcontacts","tc_clock","tc_signal","best_affinity","FDCselected_clock")
  data.frame(Time=dataset$days,lifetime=dataset$FDCselected_clock)
}

# remove the message that some data were not plotted
# options(warn=-1) 
# set the output file name
file <- "apoptoticBClifetime.eps"
cairo_ps(width=10, height=10, file)
# read the data
alldata <- get_data("./","apolog.out")
print(alldata)
# get the range of lifetimes
alllifetimes <- unique(alldata$lifetime)
Nalllifetimes <- length(alllifetimes)
minlifetime <- 0
maxlifetime <- 5 #alllifetimes[Nalllifetimes]
nbreaks <- 50
MaxFreq <- 3000
par(mai=c(1.1,1.3,0.4,0.2)) 
hist(alldata$lifetime, breaks=nbreaks, col="red", 
     xlim=c(minlifetime,maxlifetime), ylim=c(0,MaxFreq), 
     cex.lab=2.5, cex.axis=2.0, cex=2.0, xlab="", ylab="", main="")
title(xlab="Acquired time of B cell search for Tfh [hours]", cex=2.0, cex.lab=2.5, line=4)
title(ylab="Frequency", cex=2.0, cex.lab=2.5, line=4)
dev.off()
##################################################################################################
