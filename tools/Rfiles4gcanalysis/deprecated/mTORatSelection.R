##################################################################################################
# generates a histogram of the mTOR levels per B cell at the time of selection.
# Data are collected from mTORatSelection.out 
# in column 2 (Signal level) and 3 (frequency) and 4-25 (frequencies at days)
##################################################################################################
get_data <- function(pathname,filename) {
  dataset <- read.table(paste(pathname,filename,sep=""), header=FALSE, skip=1)
  colnumber <- ncol(dataset) - 4
  resultname <- c("BIN","SIG","FREQ")
  resultname <- c(resultname, sprintf("D%i", 0:colnumber))
  names(dataset) <- resultname
  dataset
}

# remove the message that some data were not plotted
# options(warn=-1) 
daynumber = 21
# set the output file name
file <- "mTORatSelection.eps"
print(file)
cairo_ps(width=10, height=10, file)
# read the data
alldata<- get_data("./","mTORatSelection.out")
datatmp <- alldata[ which(alldata$FREQ>0), ]
alldata$FREQ<- alldata$FREQ + 0.1
MaxSig <- max(datatmp$SIG)+1
MaxFreq <- max(alldata$FREQ)
par(mai=c(1.1,1.3,0.4,0.2), col="black", lwd=2) 
# generate the graph and plot the first curve
plot(alldata$SIG, alldata$FREQ, lty=1, type="l", col="black", log="y",
         cex.axis=2.0, cex.lab=2.5, cex=2.0, 
         xlab="", ylab="",
         xlim=c(0,MaxSig), ylim=c(0.1,MaxFreq))
title(xlab="mTOR level at time of B cell selection [a.u.]", 
      cex=2.0, cex.lab=2.5, line=4)
title(ylab="Frequency", cex=2.0, cex.lab=2.5, line=4)
par(col="black")
#legend(0.5, 8e+04, inset=.02, c("dead B cells","selected B cells"), 
#       lty=c(2,1), bty="n", cex=2, horiz=FALSE)
plot_col <- rainbow(daynumber+1)
daynames <- c()
plotted_cols <- c()
daylist <- list(1,2,3,5,7,10,15,21)
#for (days in 0:21) {
for (days in daylist) {
  tmpdayname <- paste("Day ",days,sep="")
  daynames <- c(daynames,tmpdayname)
  plotted_cols <- c(plotted_cols, plot_col[days+1])
  alldata[,days+4] <- alldata[,days+4] + 0.1
  par(new=TRUE, lwd=2)
  plot(alldata$SIG, alldata[,days+4], lty=1, type="l", col=plot_col[days+1], log="y",
       cex.axis=2.0, cex.lab=2.5, cex=2.0, 
       xlab="", ylab="", axes=FALSE,
       xlim=c(0,MaxSig), ylim=c(0.1,MaxFreq))
}
legend("topright", 0, daynames, lty=1, lwd=2, cex=1.4, col=plotted_cols, title="GC time")
garbage <- dev.off()
##################################################################################################
