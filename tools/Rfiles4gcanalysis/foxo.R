##################################################################################################
# generates a plot of fractions of CCs with different degrees of FoxO expression
##################################################################################################
get_data <- function(pathname,filename) {
  dataset <- read.table(paste(pathname,filename,sep=""), header=FALSE, skip=1)
  names(dataset) <- c("hours","days","nCC","fCC0","fCC1","fCC2","fCC3","fCC4",
                                     "nCCna","fCC0na","fCC1na","fCC2na","fCC3na","fCC4na",
                                     "nCCun","fCC0un","fCC1un","fCC2un","fCC3un","fCC4un",
                                     "nCCse","fCC0se","fCC1se","fCC2se","fCC3se","fCC4se")
  data.frame(Time=dataset$days,
             nCC=dataset$nCC,f0=dataset$fCC0,f1=dataset$fCC1,
             f2=dataset$fCC2,f3=dataset$fCC3,f4=dataset$fCC4,
             nCCna=dataset$nCCna,f0na=dataset$fCC0na,f1na=dataset$fCC1na,
             f2na=dataset$fCC2na,f3na=dataset$fCC3na,f4na=dataset$fCC4na,
             nCCun=dataset$nCCun,f0un=dataset$fCC0un,f1un=dataset$fCC1un,
             f2un=dataset$fCC2un,f3un=dataset$fCC3un,f4un=dataset$fCC4un,
             nCCse=dataset$nCCse,f0se=dataset$fCC0se,f1se=dataset$fCC1se,
             f2se=dataset$fCC2se,f3se=dataset$fCC3se,f4se=dataset$fCC4se)
}
mk_plot <- function(xdata,ydata,curvecol,first) {
  xmin <- min(xdata)
  xmax <- max(xdata)
  # set the margins and set the line width
  if (first == TRUE) {
    par(mai=c(1,1,0.5,0.5), col="black", pch=0) 
  } else {
    par(new=TRUE, col=color[i], pch=0)
  }
  # generate the graph and plot the first curve
  plot(xdata, ydata, type="l", col=curvecolor,
       cex.axis=1.5, cex.lab=2.0, cex=1.2, 
       xlab="Time [days]", ylab="Fraction of CC", 
       xlim=c(xmin,xmax), ylim=c(0,1), axes=first)
}
# remove the message that some data were not plotted
# options(warn=-1)
# set the output file name
file <- "foxoCC.eps"
cairo_ps(width=10, height=10, file)
# read the data
alldata <- get_data("./","foxoCC.out")
#print("data read")
#print(alldata)
mintime = min(alldata$Time)
maxtime = max(alldata$Time)
# define a corresponding colour range for the lines
curvecolor <- rainbow(5)
# for each time draw an extra line 
mk_plot(alldata[,1], alldata[,3],curvecolor[0],TRUE) 
#legend(affinities[2], 1.0, times, lty=1, lwd=2, pch=0, cex=1.0, col=color, title="day", 
#legend(afflabels[2], 1.0, times, lty=1, lwd=2, pch=0, cex=1.0, col=color, title="day", 
#       text.col="black")
dev.off()
##################################################################################################
