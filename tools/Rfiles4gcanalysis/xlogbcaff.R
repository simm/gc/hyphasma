##################################################################################################################
# dotplot for clonal dominance versus color dominance times color density for different staining thresholds
##################################################################################################################
#library(ggplot2)
# remove the message that some data were not plotted
options(warn=-1)
##################################################################################################################
# set the number of thresholds to be used
tfile<-paste("xvolume.out", sep="")
tdata<-read.table(tfile, header=FALSE, skip=1)
maxtime<- max(tdata$V1)
time_list = seq(24,maxtime,by=48)
for (i in 1:length(time_list)) {
  time_this = time_list[i]
  file=paste("xlogbcaff_day",time_this/24,".eps",sep="")
  print(paste("BC affinity dotplot for time=",time_this," ... ",file))
  # set the output file name
  cairo_ps(width=10, height=10, file)
  times <- c(0)
#  # go through all Nruns
#  dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
  count=0
  min=1.0e-04
#  for (idir in dir_list) { 
    count=count+1
    # read next data set
#    ifile<-paste(idir,"/clonality.out", sep="")
    ifile<-paste("xlogbcaff.out", sep="")
    simall<- read.table(ifile, header=FALSE, skip=1)
    # Reduce the data to those at time <time_this> (which is stored in col 1)
    sim <- simall[simall[,1]==time_this,]
    # Keep only the two antigens with highest affinity (cols 4+) and save in cols 4 and 5
    ### to be done
    if (length(sim[1,])<5) {
       xsim <- 1
       ysim <- 1
       csim <- 1
    } else {
       # store cols 4 and 5 in xsim and ysim
       xsim <- sim[,4]
       ysim <- sim[,5]
       csim <- sim[,2]+1
    }
    plotcolor = c("red", "green", "black")
    cell = c("CB", "CC", "OUT")
    symbol = c(0,1, 2)
    jf <- 10
    if (count==1) {
       # set the margins and set the line width
       par(mai=c(1,1,0.5,0.5), col="black") 
       # generate the graph and plot the first curve
       plot(jitter(xsim, factor = jf), jitter(ysim, factor = jf), log="xy", col=plotcolor[csim], 
	pch=csim-1, type="p", 
	cex.axis=1.2, cex.lab=1.5, cex=1.2,  
	xlab="binding probability to epitope 1", ylab="binding probability to epitope 2",
        xlim=c(min,1.1), ylim=c(min,1.1))
       title(main = paste("BC affinity distribution to the 2 epitopes with highest affinity at day ",time_this/24,sep=""))
    } else {
       # prevent opening of a new graph and set the color
       par(new=TRUE, col=FALSE)
       # plot without remaking axes
       plot(jitter(xsim, factor = jf), jitter(ysim, factor = jf), log="xy", col=plotcolor[csim], 
	pch=csim-1, type="p", 
	cex=1.2, xlab="", ylab="", axes=FALSE, xlim=c(min,1.1), ylim=c(min,1.1))
    }
  #}
  legend(0.0001, 0.0003, cell, pch=symbol, lwd=2, col=plotcolor, title="cell")
  dev.off()
}
##################################################################################################################
