// #include <stdlib.h>
// #include <ctime>
// #include <stdio.h>
// #include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
using namespace std;

/*
 * ====================================================================================
 * ====================================================================================
 *
 * ====================================================================================
 * ====================================================================================
 */

int char2int(char * c) {
  int i = 0;
  int r = 0;
  while (int (c[i]) - 48 >= 0 && int (c[i]) - 48 <= 9) {
    r *= 10;
    r += int (c[i]) - 48;
    ++i;
  }
  return r;
}
int main(int argn, char * * argument) {
  if (argn != 5) {
    cout << "This programme expects the following command line parameters:\n"
         << "   1) the name of the file to be reduced in resolution,\n"
         << "   2) the number of lines to be ignored at the beginning of the file,\n"
         << "   3) the total number of columns in the file,\n"
         << "   4) the number of values to be grouped to an average value.\n\n";
    exit(1);
  }
  cout << "Reduce resolution of file " << argument[1] << "\n";

  // for (int i=0; i<5; i++) cout<<argument[i]<<"\n";

  int ignore = char2int(argument[2]);
  cout << "Ignore the first " << ignore << " lines.\n";

  int columns = char2int(argument[3]);
  cout << "Assume " << columns << " columns in the file.\n";

  int window = char2int(argument[4]);
  cout << "Assume equadistant points and average over " << window << " values.\n";

  // Eroeffnung der Analyse Dateien:
  ifstream input(argument[1]);
  ofstream output("averaged.out");
  cerr << "... opened files ...\n";
  string blastr = "";
  for (int i = 0; i < ignore; i++) {
    getline(input,blastr);
    cout << ";";
  }
  cerr << "\n... ignored the first " << ignore << " lines ...\n";
  double y[columns];
  double tmp = 0;
  int found = 0;
  while (input.eof() == 0) {
    found = 0;
    for (int j = 0; j < columns; j++) {
      y[j] = 0;
    }
    for (int i = 0; i < window; i++) {
      for (int j = 0; j < columns; j++) {
        if (input.eof() == 0) {
          input >> tmp;
          // cout<<tmp<<"  ";
          y[j] += tmp;
          if (j == columns - 1) { ++found; }
        }
      }
      // cout<<"\n";
    }
    if (found == window) {
      for (int j = 0; j < columns; j++) {
        if (j == 0) {
          y[j] /= double (found);
        }
        output << y[j] << "   ";
      }
      output << "\n";
    }
  }
  output.close();
  input.close();
  cout << "Only averaged first column (summed the others)!\n";
  cout << "Wrote result to averaged.out.\n";
  return 1;
}
