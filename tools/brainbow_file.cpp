// #include <stdlib.h>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "brainbow.h"

/* Author: Michael Meyer-Hermann August 22nd 2015
 *
 * Uses the file brainbow.out to track mutations.
 */

int main() {
  srand(time(NULL));
  brainbow rb;
  // set analysis parts: do_clonality, do_clone_fractions, do_clone_fractions_rand
  double stain_time = 48; 
  if (stain_time == 0) {
    // This parameter has to be > 0!!!
    cout << "Please chose stain_time > 0 in brainbow_file.cpp!\n";
    exit(1);
  }
  rb.set_do(true,true,true);
  rb.read_from_file();
  rb.set_Ncolour(10);
  rb.set_stain_time(stain_time);
  rb.set_lineage_time(stain_time);
  // 1-0.5212 (for gabriel_10colours)
  // 0.4 (for gabriel_4colours or gabriel_10colours)
  // 1-0.1089 for gabriel_10colours_2clone
  rb.set_stain_fraction(1-0.5212);
  rb.set_stain_dt(1); // 1.0/2.0; // hours
  rb.set_tamoxifen_decay(-1); // (-1); (24);
  rb.set_include_late_founders_in_lineages(false);
  rb.set_include_seclargest_colour(false);
  rb.set_tamoxifen_stop_time(stain_time + 48); // 48, 60);
  //rb.set_staining_mode(brainbow::gabriel_4colours);
  rb.set_staining_mode(brainbow::gabriel_10colours);
  rb.set_origin_same_colour(false);
  rb.set_merge_identical_origin(false);
  rb.set_allow_restaining(false);
  rb.set_cell_subset_size(70);
  double deltat = 24; // 24 96;
  int Nevals = 20; // 20; //14;
  // int Nevals=5;
  double eval_times[Nevals];
  for (int i = 0; i < Nevals; ++i) {
    eval_times[i] = stain_time + (double (i) * deltat);
  }
  // Use these two lines if you want to give irregular evaluation times:
  // eval_times[0]=3; eval_times[1]=6; eval_times[2]=9; eval_times[3]=12; eval_times[4]=17;
  // for (int i=0; i<Nevals; ++i) eval_times[i]*=24.;
  // Remove these previous two lines for regular evaluation times.
  rb.make_brainbow(eval_times, Nevals);
  rb.save_Muller_diagram();
  return 1;
}
