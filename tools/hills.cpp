#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "setparam.h"
//#include "cellman.h"

void show_const(double min, double max, double x, ofstream& ff) {
  ff << min << "  " << x << "  " << x << endl;
  ff << max << "  " << x << "  " << x << endl;
}
void calc_mTOR_BCR(ofstream& hills, Werte& p) {
  cout << "Generate hill_mTOR_BCR.out\n";
  hills.open("hill_mTOR_BCR.out");
  hills << "BCR-signal  Hill  mTOR-production\n";
  if (p.mTOR_mode == 2) { // implies FoxO_mode > 3
    /* mTOR_upregulation_rate_modulation(BCR signal level)
     *
     * This corresponds to H_R(B) in the FCRB-model equations.
     * It contributes to the Tfh-independent mTOR upregulation and
     * to modulates Tfh-signals during interactions with Tfh.
     * The Hill-function is shown as the Tfh-independent production time to reach mTOR==1.
     * The Hill-function itself is also shown and used for the Tfh-dependent part
     * as fraction of maximum production rate. However, here other modulating
     * factors like ICOSL may further change these values, such that only the Hill-function
     * itself makes sense.
     */
    show_Hill(p.dT_mTORC1, 0, p.mTOR_BCR_K, p.mTOR_BCR_n, true, 1, 100, hills);
  } else { // mTOR_mode==1 || ==0
    show_const(0.1,100,p.dT_mTORC1,hills);
  }
  hills.close();
}  
void calc_FoxO_BCR(ofstream& hills, Werte& p) {
  cout << "Generate hill_FoxO_BCR.out\n";
  hills.open("hill_FoxO_BCR.out");
  hills << "BCR-signal  Hill  FoxO-production\n";
  if (p.FoxO_mode == 4 && p.dT_FoxO_hill == 1 && p.tc_search_duration_mode == 5) {
    /* FoxO_downregulation_rate_modulation(BCR signal level)
     *
     * This corresponds to H_F(B) in the FCRB-model equations.
     * It contributes to the BCR-dependent inhibition of FoxO,
     * which subsequently releases the FoxO-break on cMyc upregulation.
     * The Hill-function is meant as fraction of maximum reduction rate \alpha_F. 
     * The first version below shows real times of regulation here.
     * The second version simply shows the regulation as factor [0,1].
     */
    show_Hill(p.dT_FoxO, 0, p.dT_FoxO_K, p.dT_FoxO_n, true, 1, 100, hills);
    //show_Hill(0, 1, p.dT_FoxO_K, p.dT_FoxO_n, true, 1, 100, hills);
  } else {
    show_const(0.1,100,-1,hills);
  }
  hills.close();
}  
void calc_FoxO_pMHC(ofstream& hills, Werte& p) {
  cout << "Generate hill_FoxO_pMHC.out\n";
  hills.open("hill_FoxO_pMHC.out");
  hills << "pMHC  Hill  FoxO-production\n";
  if (p.dT_FoxO_hill == 0 || (p.FoxO_mode != 1 && p.FoxO_mode != 2)) {
    if (p.tc_search_duration_mode == 3 || p.tc_search_duration_mode == 4) {
      show_const(0.1,100,p.dT_FoxO,hills);
    } else { // FoxO not used
      show_const(0.1,100,-1,hills);
    }
  } else if (p.FoxO_mode == 2) { // i.e. p.dT_FoxO_hill == 1 && tc_search_duration_mode == 3
    /* FoxO_upregulation_rate_modulation(pMHC)
     *
     * The more pMHC, the faster FoxO is upregulated.
     * All this in the FoxO-controlled differentiation model.
     * Showing real times of regulation here.
     */
    show_Hill(p.dT_FoxO_max, p.dT_FoxO_min, p.dT_FoxO_K, p.dT_FoxO_n, true, 1, 100, hills);
  } else if (p.FoxO_mode == 1) {
    /* FoxO_upregulation_rate_modulation(pMHC)
     * The more pMHC, the slower FoxO is upregulated.
     * Showing real times of regulation here.
     */
    show_Hill(p.dT_FoxO_min, p.dT_FoxO_max, p.dT_FoxO_K, p.dT_FoxO_n, true, 1, 100, hills);
  }
  hills.close();
}  
void calc_FoxOdeflection_pMHC(ofstream& hills, Werte& p) {
  cout << "Generate hill_FoxOdeflection_pMHC.out\n";
  hills.open("hill_FoxOdeflection_pMHC.out");
  hills << "pMHC  Hill  FoxO-deflection\n";
  if (p.dT_FoxO_hill == 0 || p.FoxO_mode != 0 || p.tc_search_duration_mode != 3) {
    show_const(0.1,100,0,hills);
  } else if (p.FoxO_mode == 2) { // i.e. p.dT_FoxO_hill == 1 && tc_search_duration_mode == 3
    /* FoxO_upregulation_rate_modulation(pMHC)
     *
     * The more pMHC, the faster FoxO is upregulated.
     * All this in the FoxO-controlled differentiation model.
     * Showing real times of regulation here.
     */
    show_Hill(p.dT_FoxO_max, p.dT_FoxO_min, p.dT_FoxO_K, p.dT_FoxO_n, true, 1, 100, hills);
  } else if (p.FoxO_mode == 1) {
    /* FoxO_upregulation_rate_modulation(pMHC)
     * The more pMHC, the slower FoxO is upregulated.
     * Showing real times of regulation here.
     * Note that the deflection is further modulated by (1-mTOR), which is not shown.
     */
    show_Hill(0, 1, p.KFoxO, p.nFoxO, true, 1, 100, hills);
  }
  hills.close();
}  
void calc_cMyc_inh_FoxO(ofstream& hills, Werte& p) {
  if (p.FoxO_mode == 4) {
    cout << "Generate hill_cMyc_FoxO.out\n";
    hills.open("hill_cMyc_FoxO.out");
    hills << "FoxO-level  Hill  inhibition-of-cMyc-production\n";
    if (p.cMyc_FoxO_break == 1) {
      /* cMyc_upregulation_inhibition(FoxO level)
       *
       * This corresponds to H_C(F) in the FCRB-model equations.
       * It contributes to the modulation of cMyc-upregulation,
       * which subsequently determines DND in the FCRB-model.
       * The Hill-function is meant as modulating factor [0,1] of inhibition
       * and enters the modulation as 1-this
       */
      show_Hill(1, 0, p.cMyc_FoxO_break_K, p.cMyc_FoxO_break_n, true, 1, 100, hills);
    } else {
      show_const(0.01,1.5,0,hills);
    }
    hills.close();
  }
}  
void calc_TFHsignal_pMHC(ofstream& hills, Werte& p) {
  cout << "Generate hill_TFHsignal_pMHC.out\n";
  hills.open("hill_TFHsignal_pMHC.out");
  hills << "pMHC  Hill  Tfh-signal-factor\n";
  if (p.TFHsignal_delivery_mode == 1) {
    /* TFHsignal_delivery_factor(pMHC)
     *
     * This corresponds to a modulation of T cell signal delivery to the
     * BC which the Tfh is polarized to. Depending on the presented amount of pMHC
     * the time of interaction, which also meant to upregulate cMyc and the mTOR
     * signals in the FCRB-model, is modulated by this factor.
     * This factor enters T(p) in the FCRB-model.
     */
    double K = p.TFHsignal_delivery_KpMHC;
    if ( K < 0 ) {
      K = get_Hill_K(p.TFHsignal_delivery_pMHCof1, p.TFHsignal_delivery_min, 
		     p.TFHsignal_delivery_max, p.TFHsignal_delivery_n, 1.0);
    }
    show_Hill(p.TFHsignal_delivery_min, p.TFHsignal_delivery_max, 
	      K, p.TFHsignal_delivery_n, true, 1, 100, hills);
  } else {
    show_const(0.1,100,1,hills);
  }
  hills.close();
}
void calc_BTtime_pMHC(ofstream& hills, Werte& p) {
  cout << "Generate hill_BTtime_pMHC.out\n";
  hills.open("hill_BTtime_pMHC.out");
  hills << "pMHC  Hill  B-Tfh-interaction-duration\n";
  if (p.mode_of_setting_TC_time == 3) {
    /* BTtime(pMHC)
     *
     * The duration of interactions between B and Tfh is determined based on the
     * level of presented pMHC on the B cell.
     */
    show_Hill(p.BTtime_min, p.BTtime_max, p.BTtime_K, p.BTtime_n, true, 1, 100, hills);
  } else {
    show_const(0.1,100,p.TC_time,hills);
  }
  hills.close();
}
double get_K_DND(double K, double x0, Werte& p) {
  if (K < 0) {
    K = get_Hill_K(x0, p.pMHC_dependent_P_min, p.pMHC_dependent_P_max,
		   p.pMHC_dependent_nHill, p.pMHC_dependent_P_standard);
  }
  return K;
}
void calc_DND(ofstream& hills, Werte& p) {
  cout << "Generate hill_DND_X.out\n";
  hills.open("hill_DND_X.out");
  double K = 0; 
  bool done = false;
  if (p.pMHC_dependent_division) {
    hills << "pMHC  Hill   DND\n";
    K = get_K_DND(p.pMHC_dependent_K, p.pMHC_dependent_pMHC_of_2divisions, p);
  } else if (p.use_cMyc4DND) {
    hills << "cMyc  Hill   DND\n";
    K = get_K_DND(p.cMyc_dependent_K, p.cMyc_of_P0divisions, p);
  } else if (p.signal_dependent_number_of_divisions) {
    if (p.mode_of_DND_of_Tfh == 0) {
      hills << "Tfh-signal  Hill   DND\n";
      K = get_K_DND(p.TFHsignal_dependent_K, p.TFHsignal_of_P0divisions, p);
    } else if (p.mode_of_DND_of_Tfh == 1) {
      hills << "Tfh-signal-gradient  Hill   DND\n";
      K = get_K_DND(p.TFHgradient_dependent_K, p.TFHgradient_of_P0divisions, p);
    } else {
      cerr << "p.mode_of_DND_of_Tfh >= 2 not programmed. Exit.\n";
      exit(1);
    }
  } else {
    show_const(0.1,100,p.CB_fixed_times_of_divisions,hills);
    done = true;
  }
  if (not(done)) {
    show_Hill(p.pMHC_dependent_P_min, p.pMHC_dependent_P_max,
	      K, p.pMHC_dependent_nHill, true, 1, 100, hills);
  }
  hills.close();
}
void calc_ICOSL_t(ofstream& hills, Werte& p) {
  cout << "Generate hill_ICOSL_t.out\n";
  hills.open("hill_ICOSL_t.out");
  hills << "t  Hill  ICOSL\n";
  if (p.ICOSL_dependent_Tfh_signals == 1 && p.ICOSL_upregulation_mode == 1) {
    /* BTtime(pMHC)
     *
     * The duration of interactions between B and Tfh is determined based on the
     * level of presented pMHC on the B cell.
     */
    show_Hill(0, 1, p.ICOSL_upregulation_time, 2, true, 1, 100, hills);
  } else {
    show_const(0.1,10,1,hills);
  }
  hills.close();
}
void calc_hills(Werte& p) {
  ofstream hills;
  calc_mTOR_BCR(hills, p);
  calc_FoxO_BCR(hills, p);
  calc_FoxO_pMHC(hills, p);
  calc_FoxOdeflection_pMHC(hills, p);
  calc_cMyc_inh_FoxO(hills, p);
  calc_TFHsignal_pMHC(hills, p);
  calc_BTtime_pMHC(hills, p);
  calc_DND(hills, p);
  calc_ICOSL_t(hills, p);
}

int main(int argn, char * * argument) {
  char cross[2] = "#";
  if (argn == 1) {
    argument[1] = cross;
  }
  // Get associated parameter set:
  Parameter par;
  int done = 0; 
  if (argn <= 2) {
    done = par.wahl(argument[1],true,true);
  } else if (argn == 3) {
    par.wahl(argument[1],false,true);
    done = par.wahl(argument[2],true,false);
  } else {
    cout << "ERROR: more than 2 command line parameters not supported --> exit\n";
    exit(1);
  }
  // Start calculation if parameters successfully read
  if (done == 5) {
    calc_hills(par.Value);
    cerr << "hills finished.\n";
  } else {
    cerr << "hills aborted.\n";
  }
  return 0;
}
